- [x] Setup vim
  - [x] Install vim
  - [x] Put vimrc and other configs in the right place
  - [x] Ensure node is installed
  - [x] Ensure python is installed and pynvim is installed
  - [x] Run vim install handler
  - [ ] C/C++ environment/addon?
- [x] Setup gdb (gef)
- [x] Setup Rust
  - [x] Install rustup
  - [x] Install the appropriate versions
- [x] Setup Python
  - [x] Install and upgrade pip
  - [x] Install poetry and pipx
- [x] Configs
  - [x] Rclone
  - [ ] AWS (aws vault)
- [x] SSH Config
- [ ] Configure libreoffice
  - [ ] Install fonts
  - [ ] Set fonts
  - [ ] Set correct theme
- [ ] Configure gnome
  - [ ] Nightlight
  - [ ] Keyboard backlight
  - [ ] Don't sleep when plugged in
  - [ ] Mouse speed
  - [ ] Make sure the icons are in the right spot
  - [ ] Favorite apps
- [x] Install shit
- [ ] Put files back


**Make Sure to make an upgrade script for all the fancily installed stuff**
**And also throw in upgrades for pip, pipx, cargo shit, npm, vim, etc.**
**And go ahead and throw that in some systemd shit**

Python install
- ipython
- jupyter

flatpak (apt)
zoom (flatpak zoom-client)
obs (apt obs-studio)
cloc (or something like it)
